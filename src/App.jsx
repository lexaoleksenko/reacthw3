import React, { useState} from 'react'
import './reset.css'
import './App.scss';

import { Routes, Route} from 'react-router-dom';

import Modal from './components/modal/mod';
import Items from './components/item/items.jsx';
import Navbar from './components/navbar/navbar';
import CartPage from './components/cartPage/cart';
import SelectPage from './components/selectPage/selectPage';

function App(props){
  let [activeModal, setModal] = useState(false);
  let [items] = useState(
  [
    {
    article: 1,
    name: "Item 1",
    price: "21$",
    img: "./phph.jpg",
    color: "col1",
    },
    {
    article: 2,
    name: "Item 2",
    price: "22$",
    img: "./phph.jpg",
    color: "col2" ,
    },
    {
    article: 3,
    name: "Item 3",
    price: "23$",
    img: "./phph.jpg",
    color: "col3",
    },
    {
    article: 4,
    name: "Item 4",
    price: "24$",
    img: "./phph.jpg",
    color: "col4",
    },
    {
    article: 5,
    name: "Item 5",
    price: "35$",
    img: "./phph.jpg",
    color: "col5",
    },
    {
    article: 6,
    name: "Item 6",
    price: "36$",
    img: "./phph.jpg",
    color: "col6", 
    },
    {
    article: 7,
    name: "Item 7",
    price: "37$",
    img: "./phph.jpg",
    color: "col7", 
    },
    {
    article: 8,
    name: "Item 8",
    price: "48$",
    img: "./phph.jpg",
    color: "col8", 
    },
    {
    article: 9,
    name: "Item 9",
    price: "49$",
    img: "./phph.jpg",
    color: "col9", 
    },
    {
    article: 10,
    name: "Item 10",
    price: "52$",
    img: "./phph.jpg",
    color: "col10", 
    },
    {
    article: 11,
    name: "Item 11",
    price: "21$",
    img: "./phph.jpg",
    color: "col1",
    },
    {
    article: 12,
    name: "Item 12",
    price: "22$",
    img: "./phph.jpg",
    color: "col2" ,
    },
    {
    article: 13,
    name: "Item 13",
    price: "23$",
    img: "./phph.jpg",
    color: "col3",
    },
    {
    article: 14,
    name: "Item 14",
    price: "24$",
    img: "./phph.jpg",
    color: "col4",
    },
    {
    article: 15,
    name: "Item 15",
    price: "35$",
    img: "./phph.jpg",
    color: "col5",
    },
    {
    article: 16,
    name: "Item 16",
    price: "36$",
    img: "./phph.jpg",
    color: "col6", 
    },
    {
    article: 17,
    name: "Item 17",
    price: "37$",
    img: "./phph.jpg",
    color: "col7", 
    },
    {
    article: 18,
    name: "Item 18",
    price: "48$",
    img: "./phph.jpg",
    color: "col8", 
    },
    {
    article: 19,
    name: "Item 19",
    price: "49$",
    img: "./phph.jpg",
    color: "col9", 
    },
    {
    article: 20,
    name: "Item 20",
    price: "52$",
    img: "./phph.jpg",
    color: "col10"
    }
  ]
  )
  let [cartItems, setCartItems] = useState([]);
  let [cartItem, setCartItem] = useState();
  let [selectItems, setSelectItems] = useState([]); 


  function toggleModal(item) {
    const currentState = activeModal;
    setModal( activeModal = !currentState );
    setCartItem(cartItem = item)
  };

  function addCart(){
    setCartItems(cartItems = [...cartItems, cartItem], setModal(activeModal = false));
    localStorage.setItem(`cartItem${cartItem.article}`, JSON.stringify(cartItem));
  }

  function delCart(item){
    for (let i = 0, len = cartItems.length; i < len; i++) {
      if (cartItems[i].article === item.article) {
        cartItems.splice(i, 1);
        setCartItems(cartItems = [...cartItems])
        localStorage.removeItem(`cartItem${item.article}`);
        break;
      }
    }
  }
  
  function addSelect(item){
    if(selectItems.includes(item)){
      for (let i = 0, len = selectItems.length; i < len; i++) {
        if (selectItems[i].article === item.article) {
          selectItems.splice(i, 1);
          setSelectItems(selectItems = [...selectItems])
          localStorage.removeItem(`selectItem${item.article}`);
          break;
        }
      }
    } else {
        let selItm = ()=>{
        setSelectItems(selectItems = [...selectItems, item]);
        localStorage.setItem(`selectItem${item.article}`, JSON.stringify(item));
      }
      selItm()
    }
  }

  return(
      <>
        <Navbar selLen={selectItems.length} cartLen={cartItems.length}/>
        <Routes>
          <Route path='/' element={
          <div className='backgr'>
          <Modal styleMod={activeModal ? 'flex' : 'none'} title ="Add item to cart?" text = {`You sure? The price of this item:`}  cls = {toggleModal} enter={addCart}/>
          <Items items={items} addItem={toggleModal} select = {addSelect}/>
          </div>}
          />
          <Route path='/cartPage' element={<CartPage cartItems={cartItems} delCart={delCart}/>}/>
          <Route path='/selectPage' element={<SelectPage selItems={selectItems} delSel={addSelect}/>}/>
        </Routes>
      </>
    )
}

export default App;