import React from 'react';
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { NavLink } from 'react-router-dom';


function SelectItem(props){
    return (
        <>
            <NavLink to="/selectPage"><FontAwesomeIcon icon={faStar} style={{color: props.starState}} className='navbar__select_icon'/></NavLink>
            <div className='navbar__select_counter'>{props.selectLenght}</div>
        </>
    )
}

export default SelectItem