import React from 'react';
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { NavLink } from 'react-router-dom';

function Cart(props){
    return (
        <>
        <NavLink to="/cartPage">
            <FontAwesomeIcon icon={faCartShopping} className='navbar__cart_icon'/>
        </NavLink>
        <div className='navbar__cart_counter'>{props.cartLenght}</div>
        </>
    )
}

export default Cart