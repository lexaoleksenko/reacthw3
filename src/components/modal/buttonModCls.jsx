import React from 'react';

function ButtonModCls(props){
    return (
        <button onClick={props.onCloseClick}  className='btnMod' style={{backgroundColor: props.btnBgCol, color: props.btnCol}}>{props.btnTxt}</button>
    )
}

export default ButtonModCls