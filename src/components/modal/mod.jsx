import React, {useState} from 'react';
import ButtonModCls from './buttonModCls';
import ButtonModEnt from './buttonModEnt';

import { faCircleXmark} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Modal(props){
    let [closeButton] = useState(false);

    return (
        <div className='modal' style={{display: props.styleMod}}>
        <div className='modOver' onClick={props.cls}></div>
        <div className='modWind'>
            <div className='modHeader'>
                <div className='headerTitle'>{props.title}</div>
                <FontAwesomeIcon icon={faCircleXmark} className='headerClose' onClick={props.cls} style={{display: closeButton === true ? "block" : "none", color: "#000000" }}/>
            </div>
            <div className='modText'>{props.text}</div>
            <div className='modFooter'>
                <ButtonModCls onCloseClick={props.cls} btnTxt="Cancel"/>
                <ButtonModEnt onEnterClick={props.enter ? props.enter : ()=>{props.delCart(props.delItem)}} btnTxt="Enter" btnBgCol="#696969" btnCol="#FFFFFF"/>
            </div>
        </div>
        </div>
    )

}

    
    Modal.defaultProps = {
        title: "Modal title!",
        text: "22222222222",
        onSubmit: () => {},
    }

export default Modal