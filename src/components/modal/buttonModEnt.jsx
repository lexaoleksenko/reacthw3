import React from 'react';

function ButtonModEnt(props){
    return (
        <button onClick={props.onEnterClick}  className='btnMod' style={{backgroundColor: props.btnBgCol, color: props.btnCol}}>{props.btnTxt}</button>
    )
}

export default ButtonModEnt