import React from 'react';

import Items from '../item/items';


function SelectPage(props){
    return (
        <>
        <div className='backgr'>
            <Items selItems={props.selItems} delSel={props.delSel}/>
        </div>
        </>
    )
}

export default SelectPage