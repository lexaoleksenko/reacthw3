import React from 'react';

import Items from '../item/items';

function CartPage(props){
    return (
        <>
        <div className='backgr'>
            <Items cartItems={props.cartItems} delCart={props.delCart} />
        </div>
        </>
    )
}
export default CartPage