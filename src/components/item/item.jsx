import React, { useState,} from 'react';
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleXmark} from "@fortawesome/free-solid-svg-icons";
import Modal from '../modal/mod';

function Item(props){
    let [starState, setStarState] = useState(false);
    let [activeModal, setActiveModal] = useState(false);
    let [delItem, setDelItem] = useState({}); 

    function toggleStar() {
        const currentState = starState;
        setStarState(starState = !currentState);
    };

    function toggleModal(item) {
        const currentState = activeModal;
        setActiveModal(activeModal = !currentState);
        setDelItem(delItem = item)
    };

    if(!props.delCart && !props.delSel){
        return (
            <>
                <div className='items__item'>
                    <h2 className='items__item_name'>{props.item.name}</h2>
                    <img className='items__item_img' src={props.item.img} alt="#" />
                    <span className='items__item_price'>{props.item.price}</span>
                    <div>
                        <button className='items__item_btn' onClick={()=>{props.addItem(props.item)}}>Add to cart</button>
                        <FontAwesomeIcon className='items__item_star' icon={faStar} onClick={()=>{props.select(props.item); toggleStar()}} style={{color: starState ? "#FFFF00" : "#000000"}}/>
                    </div>
                    
                </div>
            </>
        )
    } else {
        return (
            <>
                <Modal styleMod={activeModal ? 'flex' : 'none'} title ="Delete this item ?" text = {`Are you sure you want to delete ${delItem.name}?`} delCart={props.delCart} delItem={delItem} cls = {toggleModal}/>
                <div className='items__item'>
                    <h2 className='items__item_name'>{props.item.name}</h2>
                    <img className='items__item_img' src={props.item.img} alt="#" />
                    <span className='items__item_price'>{props.item.price}</span>
                    <div>
                        {props.delCart ? <FontAwesomeIcon icon={faCircleXmark} onClick={()=>{toggleModal(props.item)}} /> : <FontAwesomeIcon icon={faStar} onClick={()=>{props.delSel(props.item)}}/>}
                    </div>
                    
                </div>
            </>
        )
    }
}
    
export default Item