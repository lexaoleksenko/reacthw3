import React from 'react';
import Item from './item';

function Items(props){
    if(props.items){
        return (
        <>
            <div className='items'>
                {props.items.map((el) => (
                    <Item item={el} key={el.article} addItem={props.addItem} select={props.select} />
                ))}
            </div>
            
        </>
    )
    } else if(props.cartItems){
        return (
        <>
            <div className='items'>
                {props.cartItems.map((el) => (
                    <Item item={el} key={el.article} addItem={props.addItem} select={props.select} delCart={props.delCart}/>
                    ))}
            </div>
        </>
    )
    } else if(props.selItems){
        return (
        <>
            <div className='items'>
                {props.selItems.map((el) => (
                    <Item item={el} key={el.article} addItem={props.addItem} select={props.select} delSel={props.delSel}/>
                    ))}
            </div>
        </>
    )}
}

export default Items